unit travelpoints;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, IniFiles;


type

      { TTravelPoint }

      TTravelPoint = class(TObject)
        private
          msStation      : String;
          mlDistance     : Longint;
          mdtArriveTime  : TDateTime;
          miStopDuration : Integer;

        public
          constructor Load(poIni : TIniFile; piIdx : Integer);
          constructor Create(psStation : String; plDistance : Longint;
                             pdtArriveTime : TDateTime; piStopDuration : Integer);
          procedure setStation(psValue : String);
          procedure setDistance(plValue : Longint);
          procedure setArriveTime(pdtValue : TDateTime);
          procedure setStopDuration(piValue : Integer);
          function  getStation : String;
          function  getDistance : Longint;
          function  getArriveTime : TDateTime;
          function  getStopDuration : Integer;
          procedure saveToIni(poIni : TIniFile; piIdx : Integer);
      end;

const csPointSection      = 'points';
      csPointStation      = 'station';
      csPointDistance     = 'distance';
      csPointArriveTime   = 'arrivetime';
      csPointStopDuration = 'stopduration';

implementation

{ TTravelPoint }

constructor TTravelPoint.Load(poIni: TIniFile; piIdx: Integer);
var lsIdx : String;
    ldtTime : TDateTime;
begin

  lsIdx:=IntToStr(piIdx);
  ldtTime:=StrToTime('00:00');
  msStation:=poIni.ReadString(csPointSection,csPointStation+lsIdx,'Ошибка!');
  mlDistance:=poIni.ReadInteger(csPointSection,csPointDistance+lsIdx,0);
  mdtArriveTime:=poIni.ReadTime(csPointSection,csPointArriveTime+lsIdx,ldtTime);
  miStopDuration:=poIni.ReadInteger(csPointSection,csPointStopDuration+lsIdx,0);
end;


constructor TTravelPoint.Create(psStation: String; plDistance: Longint;
  pdtArriveTime: TDateTime; piStopDuration: Integer);
begin

  msStation:=psStation;
  mlDistance:=plDistance;
  mdtArriveTime:=pdtArriveTime;
  miStopDuration:=piStopDuration;
end;


procedure TTravelPoint.setStation(psValue: String);
begin

  msStation:=psValue;
end;


procedure TTravelPoint.setDistance(plValue: Longint);
begin

  mlDistance:=plValue;
end;


procedure TTravelPoint.setArriveTime(pdtValue: TDateTime);
begin

  mdtArriveTime:=pdtValue;
end;


procedure TTravelPoint.setStopDuration(piValue: Integer);
begin

  miStopDuration:=piValue;
end;


function TTravelPoint.getStation: String;
begin

  Result:=msStation;
end;


function TTravelPoint.getDistance: Longint;
begin

  Result:=mlDistance;
end;


function TTravelPoint.getArriveTime: TDateTime;
begin

  Result:=mdtArriveTime;
end;


function TTravelPoint.getStopDuration: Integer;
begin

  Result:=miStopDuration;
end;



procedure TTravelPoint.saveToIni(poIni: TIniFile; piIdx : Integer);
var lsIdx : String;
begin

  lsIdx:=IntToStr(piIdx);
  poIni.WriteString(csPointSection,csPointStation+lsIdx,msStation);
  poIni.WriteInteger(csPointSection,csPointDistance+lsIdx,mlDistance);
  poIni.WriteTime(csPointSection,csPointArriveTime+lsIdx,mdtArriveTime);
  poIni.WriteInteger(csPointSection,csPointStopDuration+lsIdx,miStopDuration);
end;


end.

