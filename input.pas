unit input; 

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Buttons, ComCtrls, DateUtils, TravelPoints;

type

  { TfmInput }

  TfmInput = class(TForm)
    bbtOk: TBitBtn;
    bbtCancel: TBitBtn;
    edArriveTime: TEdit;
    edStopDuration: TEdit;
    edStation: TEdit;
    edDistance: TEdit;
    Label1: TLabel;
    lbLongtitude: TLabel;
    lbLongtitude1: TLabel;
    lbLongtitude2: TLabel;
    Panel1: TPanel;
    udDistance: TUpDown;
    udStopDuration: TUpDown;
    procedure bbtOkClick(Sender: TObject);
    procedure lbLongtitudeClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    procedure doAppend;
    procedure doEdit(poPoint : TTravelPoint);
  end; 

var
  fmInput: TfmInput;

implementation

{$R *.lfm}

{ TfmInput }

procedure TfmInput.lbLongtitudeClick(Sender: TObject);
begin

end;


procedure TfmInput.doAppend;
begin

  edArriveTime.Text:='00:00';
  udDistance.Position:=0;
  udStopDuration.Position:=0;
  edStation.Text:='';

end;

procedure TfmInput.doEdit(poPoint: TTravelPoint);
begin

  edStation.Text:=poPoint.getStation;
  udDistance.Position:=poPoint.getDistance;
  udStopDuration.Position:=poPoint.getStopDuration;
  edArriveTime.Text:=TimeToStr(poPoint.getArriveTime);
end;


procedure TfmInput.bbtOkClick(Sender: TObject);
begin

  try

    StrToDateTime(edArriveTime.Text);
  except
    edArriveTime.Text:='Неправильное время';
    ModalResult:=mrNone;
  end;
end;

end.

