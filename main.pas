unit main;

{$mode delphi}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Buttons, Inifiles,
  input,travelpoints;

type

  { TfmMain }

  TfmMain = class(TForm)
    bbtAdd: TBitBtn;
    bbtInsert: TBitBtn;
    bbtDel: TBitBtn;
    bbtSave: TBitBtn;
    bbtLoad: TBitBtn;
    bbtCreate: TBitBtn;
    bbtChange: TBitBtn;
    lbPoints: TListBox;
    dlgOpen: TOpenDialog;
    Panel1: TPanel;
    dlgSave: TSaveDialog;
    procedure bbtAddClick(Sender: TObject);
    procedure bbtChangeClick(Sender: TObject);
    procedure bbtCreateClick(Sender: TObject);
    procedure bbtDelClick(Sender: TObject);
    procedure bbtInsertClick(Sender: TObject);
    procedure bbtLoadClick(Sender: TObject);
    procedure bbtSaveClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
    moPointList : TList;
  public
    { public declarations }
    procedure createNewTravel;

  end; 

const csDelimiter   = ':';
      csListSection = 'list';
      csListCount   = 'count';

var
  fmMain: TfmMain;

implementation

{$R *.lfm}

{ TfmMain }

procedure TfmMain.bbtAddClick(Sender: TObject);
var loPoint : TTravelPoint;
begin

  fmInput.doAppend();
  if fmInput.ShowModal=mrOk then
  begin

    loPoint:=TTravelPoint.Create(fmInput.edStation.Text,
                                 fmInput.udDistance.Position,
                                 StrToDateTime(fmInput.edArriveTime.Text),
                                 fmInput.udStopDuration.Position);
    moPointList.Add(loPoint);
    lbPoints.Items.Add(loPoint.getStation);
  end;
end;


procedure TfmMain.bbtChangeClick(Sender: TObject);
var loPoint : TTravelPoint;
begin

  if lbPoints.ItemIndex>=0 then begin
    loPoint:=moPointList.Items[lbPoints.ItemIndex];
    fmInput.doEdit(loPoint);
  end;
end;


procedure TfmMain.bbtCreateClick(Sender: TObject);
begin

  createNewTravel;
end;


procedure TfmMain.bbtDelClick(Sender: TObject);
begin

  if lbPoints.ItemIndex>=0 then
  begin

    moPointList.Delete(lbPoints.ItemIndex);
    lbPoints.Items.Delete(lbPoints.ItemIndex);
  end;
end;


procedure TfmMain.bbtInsertClick(Sender: TObject);
var loPoint : TTravelPoint;
begin

  if lbPoints.ItemIndex>=0 then
  begin

    if fmInput.ShowModal=mrOk then
    begin

      loPoint:=TTravelPoint.Create(fmInput.edStation.Text,
                                   fmInput.udDistance.Position,
                                   StrToDateTime(fmInput.edArriveTime.Text),
                                   fmInput.udStopDuration.Position);
      moPointList.Insert(lbPoints.ItemIndex,loPoint); //Add(loPoint);
      lbPoints.Items.Insert(lbPoints.ItemIndex,loPoint.getStation);
    end;

  end;

end;

procedure TfmMain.bbtLoadClick(Sender: TObject);
var loIni   : TIniFile;
    liIdx   : Integer;
    loPoint : TTravelPoint;
    liCount : Integer;
begin

  dlgOpen.InitialDir:=ExtractFileDir(Application.ExeName);
  if dlgOpen.Execute then begin

    moPointList.Clear;
    lbPoints.Items.Clear;
    loIni:=TIniFile.Create(dlgOpen.FileName);
    liCount:=loIni.ReadInteger(csListSection,csListCount,0);
    for liIdx:=0 to Pred(liCount) do begin

      loPoint:=TTravelPoint.Load(loIni,liIdx);
      moPointList.Add(loPoint);
      lbPoints.Items.Add(loPoint.getStation);
      //loPoint.saveToIni(loIni,liIdx);
    end;
    FreeAndNil(loIni);
  end;

end;

procedure TfmMain.bbtSaveClick(Sender: TObject);
var loIni   : TIniFile;
    liIdx   : Integer;
    loPoint : TTravelPoint;
begin

  dlgSave.InitialDir:=ExtractFileDir(Application.ExeName);
  if dlgSave.Execute then begin


    loIni:=TIniFile.Create(dlgSave.FileName);
    loIni.WriteInteger(csListSection,csListCount,moPointList.Count);
    for liIdx:=0 to Pred(moPointList.Count) do begin

      loPoint:=moPointList.Items[liIdx]; //IndexOf(,liIdx);
      loPoint.saveToIni(loIni,liIdx);
    end;
    FreeAndNil(loIni);
  end;
end;


procedure TfmMain.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin

  FreeAndNil(moPointList);
end;


procedure TfmMain.FormCreate(Sender: TObject);
begin

  createNewTravel;
end;


procedure TfmMain.createNewTravel;
begin

  FreeAndNil(moPointList);
  moPointList:=TList.Create;
  lbPoints.Items.Clear;
end;


end.

